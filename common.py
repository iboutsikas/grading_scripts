from typing import List, Union
import pandas as pd
from pathlib import Path

class Config():
    """
    You do not need to modify this class to change behavior

    Look further down to set these properties.
    """
    base_path: str
    template_name: str
    repositories_path: str
    grades_path: str
    sparse_clone: bool
    project: str

    def __init__(self, dict):
        for k, v in dict.items():
            self.__dict__[k] = v

class Student():
    github_id: str
    email: str

    def __init__(self, github_id: str, email: str):
        self.github_id = github_id
        self.email = email
    
    def __str__(self):
        return f'github_id: {self.github_id}, email: {self.email}'

    def __repr__(self):
        return f'github_id: {self.github_id}, email: {self.email}'

"""
Base path can be wherever you like. Repositories, grades, etc will
be in here
"""
base_path = 'project0'

"""
You can modify the values in here, to adjust the scripts to your needs
"""
config: Config = Config({
    'base_path': base_path,
    'template_name': 'rubric_template.txt',
    'repositories_path': f'{base_path}/repositories',
    'grades_path': f'{base_path}/grades',
    'sparse_clone': True,
    'project' : 'project0'
})


def resolve_students(file_path: Union[str, Path]) -> List[Student]:
    email_column = 'UMBC Email Address'
    github_column = 'GitHub Username'

    students = []

    xl_file = pd.read_excel(file_path)

    df = pd.DataFrame(xl_file, columns=[email_column, github_column])

    for index, row in df.iterrows():
        students.append(Student(row[github_column], row[email_column]))
    
    return students