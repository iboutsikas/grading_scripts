from common import resolve_students, config
import os
import git
import sys 
import colorama
import termcolor
from pathlib import Path

if __name__ == '__main__':
    colorama.init(autoreset=True)

    # Make the repositories folder, in case it is not there
    repositories_path = Path(config.repositories_path)
    os.makedirs(repositories_path, exist_ok=True)

    for student in resolve_students(""):
        repo_url = f'git@github.com:umbc-cmsc421-fa2020/{config.project}-{student.github_id}.git'
        print(f'Cloning repository: {repo_url}')
        repo = None
        try:
            target_path = Path(repositories_path,f'{config.project}-{student.github_id}')
            
            repo = git.Repo.clone_from(repo_url, target_path, sparse=config.sparse_clone)
            print(termcolor.colored('Repository cloned successfully', 'green'))

        except git.GitCommandError as e:
            print(termcolor.colored(e, 'red'), file=sys.stderr)
        finally:
            if (repo is not None):
                repo.close()