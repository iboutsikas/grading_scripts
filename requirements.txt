# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
_libgcc_mutex=0.1=main
astroid=2.4.2=py38_0
ca-certificates=2020.7.22=0
certifi=2020.6.20=py38_0
colorama=0.4.3=py_0
gitdb=4.0.5=py_0
gitpython=3.1.3=py_1
isort=5.4.2=py38_0
jinja2=2.11.2=py_0
lazy-object-proxy=1.4.3=py38h7b6447c_0
ld_impl_linux-64=2.33.1=h53a641e_7
libedit=3.1.20191231=h14c3975_1
libffi=3.3=he6710b0_2
libgcc-ng=9.1.0=hdf63c60_0
libstdcxx-ng=9.1.0=hdf63c60_0
markupsafe=1.1.1=py38h7b6447c_0
mccabe=0.6.1=py38_1
ncurses=6.2=he6710b0_1
openssl=1.1.1g=h7b6447c_0
pip=20.2.2=py38_0
pylint=2.6.0=py38_0
python=3.8.5=h7579374_1
readline=8.0=h7b6447c_0
setuptools=49.6.0=py38_0
six=1.15.0=py_0
smmap=3.0.4=py_0
sqlite=3.33.0=h62c20be_0
termcolor=1.1.0=py38_1
tk=8.6.10=hbc83047_0
toml=0.10.1=py_0
wheel=0.35.1=py_0
wrapt=1.11.2=py38h7b6447c_0
xz=5.2.5=h7b6447c_0
zlib=1.2.11=h7b6447c_3
