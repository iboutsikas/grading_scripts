from common import resolve_students, config
import os
import git
import sys 
import colorama
import termcolor
from pathlib import Path
import shutil

if __name__ == '__main__':
    colorama.init(autoreset=True)

    for student in resolve_students():
        repo_path = Path(config.repositories_path, f'{config.project}-{student.github_id}')
        
        if not repo_path.exists():
            print(termcolor.colored(f'Repository: {repo_path} does not exist. Skipping', 'red'))
            continue

        try:
            print(f'Processing repo: {repo_path}', 'green')
            repo = git.Repo(repo_path)
            # We don't really need those, but leave them here just in case
            # Restore the repo to whatever state the student pushed
            # repo.git.reset('--hard')
            # Remove any untracked files (i.e. from compiling)
            # repo.git.clean('-xfd')
            grade_path = Path(config.grades_path, f'{student.github_id}.txt')
            destination_path = Path(repo_path, 'grade.txt')
            print(f'\tCopying grade from {grade_path} to {destination_path}')
            shutil.copyfile(grade_path, destination_path)

            repo.index.add(['grade.txt'])
            repo.index.commit('Add grade')

            repo.remotes.origin.push()
            print(termcolor.colored(f'\tGrade pushed', 'green'))
        except Exception as e:
            print(termcolor.colored(f'Something went wrong: {e}', 'red'))

    




    