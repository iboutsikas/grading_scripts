from common import resolve_students, config
import jinja2
import os
from pathlib import Path

if __name__ == '__main__':
    # Load the template for the rubric
    base = Path(config.base_path)
    fileloader = jinja2.FileSystemLoader(base)
    env = jinja2.Environment(loader=fileloader)
    rubric_template = env.get_template(config.template_name)

    # Make the directories for the grade files and the repositories
    grades_path = Path(config.grades_path)
    os.makedirs(grades_path, exist_ok=True)

    for student in resolve_students(""):
        # Populate the rubric template with the student's info
        output = rubric_template.render(student=student)
        
        txtfile_path = Path(grades_path, f'{student.github_id}.txt')
        with open(txtfile_path, 'w+') as grade_file:
            grade_file.write(output)
        
        

