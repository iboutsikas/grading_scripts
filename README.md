# Grading scripts to assist with grading for 421

## General
You can edit the common.py file to modify the general behavior of these scripts. In there you will find:

### base_path
This is the base path where scripts will operate. The idea is that you change this one per project to have all the
grading work separate. You can give it an absolute or a relative path.

By default it is set to 'project0' so all the scripts will operate in that directory

### config
This is the config object. For the most part you should only ever change `base_path`, `project` and `sparse_clone` between the
projects. But you can adapt the rest to fit your workflow. As you can see most of the paths are populated based on the `base_path` variable we talked
about earlier. In addition to that you have the following keys:

* `template_name`: The name of the template that will be used to generate the rubric. The scripts will look for that in
  the `base_path` directory.
* `repositories_path`: This is where the scripts will clone all the student repositories. By default it will be
  `base_path/repositories`.
* `grades_path`: This is where the scripts will put all the grade files that will be generated from the template +
  student's information.
* `sparse_clone`: If this option is true, the scripts will make a sparse clone of the repository. This should mostly be
  useful in project0 since you don't need the whole kernel source code to grade, but still it would be nice to automate
  the pushing of the grade to github.
* `project`: This will be used to generate the repo urls when trying to clone or push the grades.
  

### resolve_students()
This is where you might wish to make modifications depending on how you are handling grading for your section. The
requirement is to return a list of dicts with the following format : `{ github_id, email }`. The naming of those
parameters is important. By default, the function will look for the specified .xlsx file and extract the data from the
corresponding columns. Feel free to modify as you see fit, as long as the return value matches the requirements.

----
## The scripts

### create_gradefiles
This script will populate the rubric template with the student's email and github username, then copy it in the grades
folder with the name: `github_username.txt`

### clone_repositories
This script will clone the repositories to the specified path in the config. The repositories will be named as:
`projectX-githubusername`. If there is an error with cloning a repository you should see the error that caused it in
red. It can be the case that students did not create the repository, or never pushed and it is empty.

### push_grades
This script will, grab each student's grade file from the grade directory, and copy it to their repository as
`grade.txt`. Then it will add it, commit it with the message `Add grade` then push it to their repository.

## Usage

The repository contains an environment.yml file that you can load with any conda distribution to get all the
required packages. You can use either `conda env create -f environment.yml` or `conda env create -f requirements.txt` to
recreate the environment. 

If you are not using conda, the requirements.txt file is the "standard" python way to share the dependencies. Although I
am not 100% how it works.
